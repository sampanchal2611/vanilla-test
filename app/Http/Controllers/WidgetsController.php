<?php

namespace App\Http\Controllers;

use App\Http\Resources\WidgetsResource;
use App\Models\widgets;
use App\Models\User;
use Illuminate\Http\Request;
use \App\Http\Requests\CreateUserRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Cookie;

class WidgetsController extends Controller {

    /**
     * Get Widgets  listing.
     * @return ALL Widgets Detail
     */
    public function index() {
        $responce = WidgetsResource::collection(widgets::all());
        return WidgetsResource::sendResponse($responce, 'UserDetail retrieved successfully.');
    }

    /**
     * login user
     *@param  name,email 
     * @return user detail 
     */
    public function login(Request $request) {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user()->makeHidden(['remember_token', 'email_verified_at', 'created_at', 'updated_at']);
            $token = $user->createToken(env('AUTH_TOKEN'))->plainTextToken;
            $cookie = Cookie('access_token', $token, 60 * 24);
            return WidgetsResource::sendResponse($user, 'login success')->withCookie($cookie);
        } else {
            return WidgetsResource::sendError('Unauthorised Login', ['error' => 'Unauthorised Login']);
        }
    }
    
        /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout() {
        $cookie = cookie::forget('access_token');
        return WidgetsResource::sendResponse($cookie, 'Successfully logged out')->withCookie($cookie);
    }
    /**
     * Newly user register.
     * @param  name,email,password
     * @return user detail
     */
    public function register(CreateUserRequest $request) {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        return WidgetsResource::sendResponse($user, 'User register successfully.');
    }

    /**
     * Store a newly created widgets in storage.
     * @param  name,description
     * @return Widgets Detail
     */
    public function store(CreateUserRequest $request) {
        $result = widgets::create($request->all());
        if (!empty($result)) {
            return WidgetsResource::sendResponse(new WidgetsResource($result), 'New UserDetail added successfully.');
        }
    }

    /**
     * Display the specified widgets.
     * @param  id,
     * @return Widgets Detail
     */
    public function show($id = array()) {
        $UserDetail = widgets::findOrFail($id);
        if (is_null($UserDetail)) {
            return WidgetsResource::sendError('UserDetail not found.');
        }
        return WidgetsResource::sendResponse(new WidgetsResource($UserDetail), 'UserDetail retrieved successfully.');
    }

    /**
     * Update the specified widgets from storage.
     *
     * @param  id,name,description.
     * @return Widgets Detail
     */
    public function update(CreateUserRequest $request) {
        $res = widgets::findOrFail($request->id);
        $res->name = $request->name;
        $res->description = $request->description;
        if ($res->save()) {
            return WidgetsResource::sendResponse(new WidgetsResource($res), 'UserDetail Update successfully.');
        }
    }

    /**
     * Remove the specified widgets from storage.
     *
     * @param  \App\Models\widgets  $widgets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $UserDetail = widgets::findOrFail($request->id);
        if ($UserDetail->delete()) {
            return WidgetsResource::sendResponse(new WidgetsResource($UserDetail), 'UserDetail Deleted successfully.');
        }
    }

}
