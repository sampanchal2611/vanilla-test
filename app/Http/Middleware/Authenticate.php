<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
    
     /**
     * Get user Request handle with token
     */
     public function handle($request, Closure $next, ...$guards)
    {
        if($jwt = $request->cookie('access_token'))
        {           
             $request->headers->set('Authorization','Bearer '.$jwt);
            
        }  
            $this->authenticate($request, $guards);
            $Xday = date('l'); 
            $response = $next($request);
            $response->header('Access-Control-Allow-Origin', '*');
            $response->header('X-Requested-With', 'XMLHttpRequest');
            $response->header('X-Day', $Xday);
            return $response;

    }
}
