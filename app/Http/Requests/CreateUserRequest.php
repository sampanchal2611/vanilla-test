<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        if ($this->email) {
            return [
                'name' => 'required|string|max:20|nullable',
                'email' => 'required|email|unique:users,email,' . $this->id,
                'password' => 'required|min:6'
            ];
        } else {
            return [
                'name' => 'required|string|max:20|nullable',
                'description' => 'required|string|max:100|nullable'
            ];
        }
    }

}
