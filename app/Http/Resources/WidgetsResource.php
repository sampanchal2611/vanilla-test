<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WidgetsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
        ];
        
      return $data;

    }
    /**
     * send success message 
     *
     * @param  result,message
     * @return response json
     */
    public static  function sendResponse($result,$message){
      
        if(!empty($result)){
            $response =[   
            "data"   => $result,
            "success" => true,
            "measage"=>$message,
        ];
        }
        return response()->json($response,200);
        
    }
     /**
     * send Error message 
     *
     * @param  error,errorMessages
     * @return response json
     */
    public static function sendError($error,$errorMessages =[], $code = 404)
    {
        $response =[
            "success" =>false,
            "message"=>$error,
        ];
        if(!empty($errorMessages))
        {
            $response['data']=$errorMessages;
        }
        return response()->json($response,$code);
    }
}
