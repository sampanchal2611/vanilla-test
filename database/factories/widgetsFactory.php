<?php

namespace Database\Factories;

use App\Models\widgets;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class widgetsFactory extends Factory
{
   /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = widgets::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' =>$this->faker->text(20),
            'description'=>$this->faker->text(100),
            //
        ];
    }
}
