<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
   

      DB::table('users')->insert(
                        array(
                                array(
                                        'name' => 'max',
                                         'email' => 'test@test.com',
                                         'password' => Hash::make('test')
                                       
                                ),
                                array(
                                        'name' => 'john',
                                        'email' => 'test@test1.com',
                                        'password' => Hash::make('test2')
                                        
                                ),
                                array(
                                        'name' => 'mary',
                                         'email' => 'test@test2.com', 
                                        'password' => Hash::make('test3')
              
                                        
                                ),
                        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
