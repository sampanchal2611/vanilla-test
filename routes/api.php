<?php
use App\Http\Controllers\WidgetsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

/* Create New User */
Route::post('/register', [WidgetsController::class,'register']);
/* User profile login */
Route::post('/login', [WidgetsController::class,'login']);

/* Authication via middleware */
Route::middleware('auth:sanctum')->group(function () {
    
    // widget listing Crud Generator
    Route::post('/usercreate',[WidgetsController::class,'store']);
    Route::get('/userlist',[WidgetsController::class,'index']);
    Route::get('/usersearch/{id}',[WidgetsController::class,'show']);
    Route::put('/userupdate/{id}',[WidgetsController::class,'update']);
    Route::delete('/userdelete/{id}',[WidgetsController::class,'destroy']);
    // widget logout User access
    Route::post('/logout', [WidgetsController::class,'logout']);
    
});



