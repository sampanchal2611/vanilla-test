<?php

namespace Tests\Feature;
use App\Http\Controllers\WidgetsController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Http\Resources\WidgetsResource;
use App\Models\widgets;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TaskApiTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
       protected $user,$widgets;
       public  function __construct(){
           
            $this->widgets = Factory(widgets::class)->create();
            $this->widgets->actingAs($this->widgets,'api');
        }
        
    public function test_create_user_register()
    {
        $data =[
           "name"=>"test",
            "email" =>"test@test.com",
            "password"=>Hash::make('testmytask')
        ];
        $response = $this->post('register',$data);
        $response->assertStatus(200);
        $res = Factory(widgets::class)->make();
         $this->widgets->register()->save($res);
    }
    public function test_user_login()
    {
        $token = $user->createToken(env('AUTH_TOKEN'))->plainTextToken;
        $cookie = Cookie('access_token', $token, 60 * 24);
        $data =[
                 "email" =>"test@test.com",
                "password"=>"testmytask"
        ];
        $response = $this->post('/login',$data);
        $response->assertStatus(200);
        $response->assertCookie($cookie);
    }
    public function test_create_user_widget()
    {
        
        $data =[
                 "name" =>"this is y tets",
                "description"=>"hello this is my fisrt test"
        ];
        $response = $this->post('/usercreate',$data);
        $response->assertStatus(200);
        $res = Factory(widgets::class)->make();
        $this->widgets->store()->save($res);
    }
     public function test_create_userlist_widget()
    {

        $response = $this->get('/userlist');
        $response->assertStatus(200);
        $response->assertJson(['data'=>$response]);
    }
     public function test_create_usersearch_widget()
    {  
 
        $response = $this->get('/usersearch');
        $response->assertStatus(200);
        $response->assertJson(['data'=>$response]);
    }
     public function test_create_userupdate_widget()
    {  
        $data =[
                 "name" =>"this is y tets",
                "description"=>"hello this is my fisrt test"
        ];
        $id=1;
        $response = $this->put('/usersearch/"'.$id.'"',$data);
        $response->assertStatus(200);
    }
    public function test_create_userdelete_widget()
    {  
        $id=1;
        $response = $this->put('/userdelete/"'.$id.'"',$data);
        $response->assertStatus(200);
    }
    
    
}
